#include "../headerfiles/game_engine.h"
#include <iostream>
#include <array>

/**
 * @brief Method to initiate a game. It sets everything up
 * so that the game is ready to be played.
 *
 */
void GameEngine::init()
{
    // Initiate instances of savehandler
    save_handler = new SaveHandler;
    // Load all games immediately to memory
    save_handler->load_savegames_from_file("savegames.sv");

    // Initiate boardprinter. It is responsible for the visuals
    board_printer = new BoardPrinter;
    // The input handler is responsible for correct input
    input = new InputHandler;
    save_handler->print_saves();
    input->pick_save_slot(save_handler);
    board_object = save_handler->load_board_state(input->get_save_slot());

    players[0] = new Player;
    players[1] = new Player;
    players[0]->color = white;
    players[1]->color = black;
    current_player_ptr = players[0];
    b_turn_change = false;
}

/**
 * @brief The game loop. Is responsible for running the game itself.
 * returns
 *
 * @return true
 * @return false
 */
bool GameEngine::game_loop()
{
    int save_slot = input->get_save_slot();
    board_printer->set_player(current_player_ptr->color);
    if (current_player_ptr->color == white)
    {
        save_handler->save_board_state(*board_object, save_slot);
    }
    while (!b_turn_change)
    {
        clear_screen();
        board_printer->print_board(*board_object);
        std::pair<int, int> select_piece = input->pick_piece(board_object, current_player_ptr); // Find a piece to move
        if (select_piece == std::pair<int, int>(120, 120))
        {
            return check_win();
        }
        if (select_piece == std::pair<int, int>(113, 113))
        {
            std::cout << " # Saving game... " << std::endl;
            save_handler->save_board_state(*board_object, save_slot);
            save_handler->write_savegames_to_file("savegames.sv");
            return quit();
        }
        clear_screen();
        board_printer->print_board_selected(board_object->getPiecePtr(select_piece), *board_object);
        std::pair<int, int> moveTo = input->pick_move_location(); // Find a location to move
        clear_screen();
        b_turn_change = board_object->move_piece(select_piece, moveTo, current_player_ptr); // makes the board handle moving.
    }
    current_player_ptr = switch_turn();
    clear_screen();
    board_object->winCondition();
    return check_win();
}

Player *GameEngine::switch_turn()
{
    b_turn_change = false;
    if (current_player_ptr->color == black)
    {
        return players[0];
    }
    else
    {
        return players[1];
    }
}
void GameEngine::clear_screen()
{
#if defined _WIN32
    system("cls");
#elif defined(__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    if (system("clear"))
        std::cout << std::endl;
#endif
}

bool GameEngine::check_win()
{
    if (board_object->b_white_alive == false)
    {
        std::cout << "Black WINS!" << std::endl;
        return true;
    }
    if (board_object->b_black_alive == false)
    {
        std::cout << "White WINS!" << std::endl;
        return true;
    }
    return false;
}

int GameEngine::quit()
{
    std::cout << "Quitting" << std::endl;
    return 2;
}

bool GameEngine::replay()
{
    // Initiate instances of savehandler
    save_handler = new SaveHandler;
    // Load all games immediately to memory
    save_handler->load_savegames_from_file("savegames.sv");

    players[0] = new Player;
    players[1] = new Player;
    players[0]->color = white;
    players[1]->color = black;
    current_player_ptr = players[0];

    // Initiate boardprinter. It is responsible for the visuals
    board_printer = new BoardPrinter;
    board_printer->set_player(current_player_ptr->color);

    // The input handler is responsible for correct input
    input = new InputHandler;
    save_handler->print_saves();
    auto valid_save_slots = save_handler->get_valid_save_slots();
    if (valid_save_slots.size() == 0)
        return false;

    int choice = -1;
    while (choice < 0 || choice > 9)
    {
        std::cout << "Pick a saveslot for replay\n > ";
        std::cin >> choice;
        if (!(std::find(valid_save_slots.begin(), valid_save_slots.end(), choice) != valid_save_slots.end()))
            choice = -1;
    }
    size_t frame = 0;
    board_object = save_handler->load_board_state(choice, frame);
    char navigate = ' ';
    while (navigate != 'q')
    {
        clear_screen();
        std::cout << "  FRAME: " << frame << std::endl;
        board_printer->print_board(*board_object);
        std::cout << "Press 'n' for next frame and 'p' for previous frame. Press 'q' to quit\n > ";
        std::cin >> navigate;
        if (navigate == 'n')
        {
            if (frame < valid_save_slots.size())
                frame++;
        }
        else if (navigate == 'p')
        {
            if (frame > 0)
                frame--;
        }
        else if (navigate == 'q')
            return false;
        board_object = save_handler->load_board_state(choice, frame);
        current_player_ptr = switch_turn();

    }
    return true;
}