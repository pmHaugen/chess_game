#include "../headerfiles/inputhandler.h"
#include <iostream>

/**
 * @brief Construct a new Input Handler:: Input Handler object
 * 
 */
InputHandler::InputHandler()
{
}

/**
 * @brief Pick_piece asks for coordinates from the player and
 * ensures they are of a correct format to be used by the game 
 * engine.
 * 
 * @param board 
 * @param current_player_ptr 
 * @return std::pair<int, int> 
 */
std::pair<int, int> InputHandler::pick_piece(Board *board, Player *current_player_ptr)
{
    char x, y;
    std::pair<int, int> cord;
    std::cout << "Select piece:               | xx to concede | q to quit and save." << std::endl;
    do
    {
        do
        {
            std::cout << "Pick coordinates (eg. 'A2'): ";
            std::cin >> x;
            if (int(x) == 113 || int(x) == 81) return std::make_pair(113, 113);
        } while (!input_letter_checker(x));
        do
        {
            //std::cout << "Pick Y: ";
            std::cin >> y;
        } while (!input_number_checker(y));
        cord = convert_to_valid(x, y);
        if (cord.first == 120 && cord.second == 120)
        {
            if (current_player_ptr->color == white)
            {
                board->b_white_alive = false;
            }
            else
            {
                board->b_black_alive = false;
            }
            return cord;
        }

    } while (board->getPiecePtr(cord) == nullptr);
    return cord;
}

/**
 * @brief Pick move location asks for move coordinates from the player 
 * and ensures that the resulting coordinates can be used by the 
 * game engine.
 * 
 * @return std::pair<int, int> 
 */
std::pair<int, int> InputHandler::pick_move_location()
{
    char x, y;
    std::cout << "Pick where to move:" << std::endl;
    do
    {
        std::cout << "Pick coordinates (eg. 'A2'): ";
        std::cin >> x;
    } while (!input_letter_checker(x));
    do
    {
        //std::cout << "Pick Y: ";
        std::cin >> y;
    } while (!input_number_checker(y));

    std::pair<int, int> cord = convert_to_valid(x, y);

    return cord;
}

/**
 * @brief Input letter checker checks that the input is sensible 
 * to be used on the x axis of a chess board.
 * 
 * @param input 
 * @return true 
 * @return false 
 */
bool InputHandler::input_letter_checker(char input)
{
    if ((int(input) >= 97 && int(input) <= 104) || (int(input) >= 65 && int(input) <= 72) || int(input == 120) || int(input) == 88)
    {
        return true;
    }
    return false;
}

/**
 * @brief input number checker checks input are sensible to be
 * used on y axis of a chess board.
 * 
 * @param input 
 * @return true 
 * @return false 
 */
bool InputHandler::input_number_checker(char input)
{
    if ((int(input) >= 49 && int(input) <= 56) || int(input) == 120 || int(input) == 88)
    {
        return true;
    }
    return false;
}

/**
 * @brief Convert to valid is responsible for converting coordinates to
 * valid coordinates to be used on a chessboard
 * 
 * @param x 
 * @param y 
 * @return std::pair<int, int> 
 */
std::pair<int, int> InputHandler::convert_to_valid(char x, char y)
{
    int new_x;
    int new_y;

    if (x < 95)
        new_x = int(x) - 64;
    else
        new_x = int(x) - 96;
    new_y = int(y) - 48;
    std::pair<int, int> cord(new_x - 1, new_y - 1);
    if ((int(x) == 120 && int(y) == 120) || (int(x) == 88 && int(y) == 88))
        return std::pair<int, int>(120, 120);

    return cord;
}

/**
 * @brief Picks a save slot that can be used for saving and loading.
 * returns true or false to tell whether the game is over or not
 * 
 * @param board 
 */
void InputHandler::pick_save_slot(SaveHandler* save_handler)
{
    int picked;
    bool b_delete = false;
    std::cout << "---------------------------------------------" << std::endl;
    std::cout << " 1: Start a new game\n 2: Load from save" << std::endl;
    std::cout << " > ";
    std::cin >> picked;
    switch (picked)
    {
    case 1:
        std::cout << "Pick a new save slot 0-9" << std::endl;
        b_delete = true;
        break;
    case 2:
        std::cout << "pick a savestate to load 0-9" << std::endl;
        break;
    }
    std::cin >> save_slot_index;
    if(b_delete == true)
    {
        save_handler->delete_save_slot(save_slot_index);
    }
}