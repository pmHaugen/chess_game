#include "../headerfiles/board.h"

// Constructor
Board::Board() {}

// Destructor. Needs to take care of all pieces to avoid memory leak
Board::~Board()
{
    for (auto piece : board)
    {
        if (piece != nullptr)
        {
            delete piece;
        }
    }
}

// Makes it possible to use coordinate on the chessboard.
int Board::indice_function(std::pair<int, int> coordinate)
{
    return 8 * coordinate.first + coordinate.second;
}

// Constructs the chessboard.
void Board::initialize_board()
{
    // Initialize Pawns
    for (int i = 0; i < 8; i++)
    {
        // White pawns
        board[indice_function({i, 1})] = new Pawn({i, 1}, color_t::white);
        // Black pawns
        board[indice_function({i, 6})] = new Pawn({i, 6}, color_t::black);
    }
    // White officers
    board[indice_function({0, 0})] = new Rook({0, 0}, color_t::white);
    board[indice_function({7, 0})] = new Rook({7, 0}, color_t::white);
    board[indice_function({1, 0})] = new Knight({1, 0}, color_t::white);
    board[indice_function({6, 0})] = new Knight({6, 0}, color_t::white);
    board[indice_function({2, 0})] = new Bishop({2, 0}, color_t::white);
    board[indice_function({5, 0})] = new Bishop({5, 0}, color_t::white);
    board[indice_function({3, 0})] = new Queen({3, 0}, color_t::white);
    // Save the white king especially
    white_king = new King({4, 0}, color_t::white);
    board[indice_function({4, 0})] = white_king;

    // Black officers
    board[indice_function({0, 7})] = new Rook({0, 7}, color_t::black);
    board[indice_function({7, 7})] = new Rook({7, 7}, color_t::black);
    board[indice_function({1, 7})] = new Knight({1, 7}, color_t::black);
    board[indice_function({6, 7})] = new Knight({6, 7}, color_t::black);
    board[indice_function({2, 7})] = new Bishop({2, 7}, color_t::black);
    board[indice_function({5, 7})] = new Bishop({5, 7}, color_t::black);
    board[indice_function({3, 7})] = new Queen({3, 7}, color_t::black);
    // Save the black king espesially
    black_king = new King({4, 7}, color_t::black);
    board[indice_function({4, 7})] = black_king;

    for (int x = 0; x < 8; x++)
    {
        for (int y = 2; y < 6; y++)
            board[indice_function({x, y})] = nullptr;
    }
}

// Makes it possible to add a single piece to a board
void Board::add_piece_to_board(std::pair<int, int> coordinate, Piece *piece_ptr)
{
    board[indice_function(coordinate)] = piece_ptr;
}

// Let's us add nullptr to the board
void Board::add_nullptr_to_board(std::pair<int,int> coordinate)
{
    board[indice_function(coordinate)] = nullptr;
}

// Returns Pointer to a piece, given its coordinate.
Piece *Board::getPiecePtr(std::pair<int, int> coordinate)
{
    return board[indice_function(coordinate)];
}

// Method to return the board array
std::array<Piece *, 64> Board::getBoard()
{
    return board;
}

// Supposed to move the pieces on the chessboard.
// void Board::move_piece(Piece *piece_ptr, std::pair<int, int> new_position)
bool Board::move_piece(std::pair<int, int> from_position, std::pair<int, int> new_position, Player *current_player_ptr)
{
    Piece *piece_ptr = getPiecePtr(from_position);
    if (piece_ptr->get_color() != current_player_ptr->color && piece_ptr != nullptr)
    {
        piece_ptr = nullptr;
        std::cout << "Not your piece! Try again" << std::endl;
    }
    if (piece_ptr == nullptr)
        return false;

    std::vector<std::pair<int, int>> legal_moves = piece_ptr->get_legal_moves(board);
    bool b_legal = false;

    for (size_t i = 0; i < legal_moves.size(); i++) // this should be a function and probably not here
    {
        if (legal_moves[i] == new_position)
        {
            std::cout << "LEGAL" << std::endl;
            b_legal = true;
            break;
        }
    }
    if (!b_legal)
    {
        std::cout << "NOT LEGAL!" << std::endl;
        return false;
    }

    // Finally check if own is checkmate, to do so temp save the board
    auto temp_board = board;

    // Perform move
    board[indice_function(from_position)] = nullptr;
    board[indice_function(new_position)] = piece_ptr;
    
    if (current_player_ptr->color == color_t::white && white_king->is_in_check(board))
    {

        board = temp_board;
        return false;
    }
    if (current_player_ptr->color == color_t::black && black_king->is_in_check(board))
    {
        board = temp_board;
        return false;
    }

    // Finally, if everything goes well, tell the piece it has moved
    piece_ptr->move(new_position);

    return true;
}

// Method for reconstructing a board given a save file
void Board::reconstruct_board(std::vector<std::pair<std::string, std::pair<int, int>>> pieces)
{
    for (size_t i = 0; i < board.size(); i++)
    {
        board[i] = nullptr;
    }
    for (auto piece : pieces)
    {
        std::string symbol = piece.first;
        std::pair<int, int> location = piece.second;
        // Black Rook
        if (symbol.compare("\u2656") == 0)
        {
            board[indice_function(location)] = new Rook(location, color_t::black);
        }
        // White Rook
        else if (symbol.compare("\u265C") == 0)
        {
            board[indice_function(location)] = new Rook(location, color_t::white);
        }
        // Black Pawn
        else if (symbol.compare("\u2659") == 0)
        {
            board[indice_function(location)] = new Pawn(location, color_t::black);
        }
        // White Pawn
        else if (symbol.compare("\u265F") == 0)
        {
            board[indice_function(location)] = new Pawn(location, color_t::white);
        }
        // Black Queen
        else if (symbol.compare("\u2655") == 0)
        {
            board[indice_function(location)] = new Queen(location, color_t::black);
        }
        // White Queen
        else if (symbol.compare("\u265B") == 0)
        {
            board[indice_function(location)] = new Queen(location, color_t::white);
        }
        // Black king
        else if (symbol.compare("\u2654") == 0)
        {
            board[indice_function(location)] = new King(location, color_t::black);
        }
        // White King
        else if (symbol.compare("\u265A") == 0)
        {
            board[indice_function(location)] = new King(location, color_t::white);
        }
        // Black Knight
        else if (symbol.compare("\u2658") == 0)
        {
            board[indice_function(location)] = new Knight(location, color_t::black);
        }
        // White Knight
        else if (symbol.compare("\u265E") == 0)
        {
            board[indice_function(location)] = new Knight(location, color_t::white);
        }
        // Black bishop
        else if (symbol.compare("\u2657") == 0)
        {
            board[indice_function(location)] = new Bishop(location, color_t::black);
        }
        // White bishop
        else if (symbol.compare("\u265D") == 0)
        {
            board[indice_function(location)] = new Bishop(location, color_t::white);
        }
    }
}

void Board::winCondition()
{
    b_white_alive = false;
    b_black_alive = false;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            Piece *temp_ptr = getPiecePtr(std::pair<int, int>(i, j));
            if (temp_ptr != nullptr)
            {
                if (temp_ptr->get_symbol() == "\u265A")
                {
                    b_white_alive = true;
                }
                if (temp_ptr->get_symbol() == "\u2654")
                {
                    b_black_alive = true;
                }
            }
        }
    }
}