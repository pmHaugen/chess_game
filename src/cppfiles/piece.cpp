#include "../headerfiles/piece.h"

// **********************
// Piece
// Constructor for piece. It simply assigns a position
Piece::Piece(std::pair<int, int> start_position, color_t init_color)
    : position(start_position), color(init_color)
{
    cardinals = {std::make_pair(1, 0), std::make_pair(0, 1), std::make_pair(-1, 0), std::make_pair(0, -1)};
    diagonals = {std::make_pair(1, 1), std::make_pair(1, -1), std::make_pair(-1, -1), std::make_pair(-1, 1)};
}
Piece::~Piece() {}

// Method for retrieving the color of the piece
color_t Piece::get_color()
{
    return color;
}

// Method for retrieving a pieces symbol
std::string Piece::get_symbol()
{
    return symbol;
}

std::pair<int, int> Piece::get_position()
{
    return position;
}

// Method for all pieces. Accepts a piece pointer (representing a square on a board)
// Returns 0 if nothing is there
// Returns 1 if same color is there
// Returns -1 if different color is there
int Piece::check_status_of_piece(Piece *piece_ptr)
{
    if (piece_ptr == 0)
        return 0;
    if (piece_ptr->get_color() == this->get_color())
        return 1;
    return -1;
}

bool Piece::is_in_check(const std::array<Piece *, 64> &board)
{
    return check_if_square_threatened(this->position, board);
}

int inline Piece::ind(std::pair<int, int> indices)
{
    return 8 * indices.first + indices.second;
}

// Function to run with a selected direction and append all legal squares in that direction
void Piece::append_legal_moves_in_selected_direction(std::vector<std::pair<int, int>> &legal_moves,
                                                     std::pair<int, int> &direction,
                                                     const std::array<Piece *, 64> &board)
{
    // Need to fetch a pointer to a piece
    Piece *piece_ptr;
    // Int to store the status of the piece looked at
    int piece_status;

    int x = position.first;
    int y = position.second;
    x += direction.first;
    y += direction.second;
    // Uses hardcoded bounds for now
    while (x >= 0 && x < 8 && y >= 0 && y < 8)
    {
        piece_ptr = board[ind({x, y})];
        piece_status = check_status_of_piece(piece_ptr);
        if (piece_status == 1) // Same color piece is there
        {
            return;
        }
        else if (piece_status == -1) // Different color piece is there
        {
            legal_moves.push_back({x, y});
            return;
        }
        else
        {
            legal_moves.push_back({x, y});
            x += direction.first;
            y += direction.second;
        }
    }
}

// Function to check and append selected square
void Piece::append_legal_move_in_selected_square(std::vector<std::pair<int, int>> &legal_moves,
                                                 std::pair<int, int> &square,
                                                 const std::array<Piece *, 64> &board)
{
    // Need to fetch a pointer to a piece
    Piece *piece_ptr;
    // Int to store the status of the piece looked at
    int piece_status;

    int x = square.first;
    int y = square.second;
    // Guard to check that the selected coordinates are legal
    if (x < 0 || x >= 8 || y < 0 || y >= 8)
    {
        return;
    }

    // Uses hardcoded bounds for now
    piece_ptr = board[ind({x, y})];
    piece_status = check_status_of_piece(piece_ptr);
    if (piece_status == 1) // Same color piece is there
    {
        return;
    }
    else if (piece_status == -1) // Different color or no piece is there
    {
        legal_moves.push_back({x, y});
        return;
    }
    else
    {
        legal_moves.push_back({x, y});
    }
}

/**
 * @brief Method for checking if a square is threatened by anything.
 * It runs through all pieces on the board to see if an opposing piece
 * threatens the selected square
 *
 * @param selected_square
 * @return true
 * @return false
 */
bool Piece::check_if_square_threatened(std::pair<int, int> selected_square,
                                       std::array<Piece *, 64> board)
{
    Piece *original_piece_ptr = board[ind(selected_square)];
    board[ind(selected_square)] = new King(selected_square, this->get_color());
    for (Piece *piece_ptr : board)
    {
        if (check_status_of_piece(piece_ptr) == -1)
        {
            if (piece_ptr->get_symbol() != "\u2654" &&
                piece_ptr->get_symbol() != "\u265A")
            {
                auto legal_moves = piece_ptr->get_legal_moves(board);
                if (std::find(legal_moves.begin(),
                              legal_moves.end(),
                              selected_square) != legal_moves.end())
                {
                    return true;
                }
            }
            else
            {
                std::pair<int, int> threat_to_check;
                for (auto direction : cardinals)
                {
                    auto square = piece_ptr->get_position();
                    std::pair<int, int> threatened_square = {square.first + direction.first,
                                                             square.second + direction.second};
                    if (threatened_square == selected_square)
                        return true;
                }
                for (auto direction : diagonals)
                {
                    auto square = piece_ptr->get_position();
                    std::pair<int, int> threatened_square = {square.first + direction.first,
                                                             square.second + direction.second};
                    if (threatened_square == selected_square)
                        return true;
                }
            }
        }
    }
    delete board[ind(selected_square)];
    board[ind(selected_square)] = original_piece_ptr;
    return false;
}

// **********************
// Rook
// Constructor for Rook
Rook::Rook(std::pair<int, int> start_position, color_t color) : Piece(start_position, color)
{
    if (color == color_t::black)
        symbol = "\u2656";
    else
        symbol = "\u265C";
}
Rook::~Rook() {}

// Move method for Rook. Simply move to the selected position
// The Rook is responsible for knowing if the move is a legal one.
void Rook::move(std::pair<int, int> new_position)
{
    position = new_position;
}

// A method to obtain all legal moves given the current position
std::vector<std::pair<int, int>> Rook::get_legal_moves(const std::array<Piece *, 64> &board)
{
    std::vector<std::pair<int, int>> legal_moves;
    // Run through all the cardinal directions since Rook
    for (std::pair<int, int> direction : cardinals)
    {
        // Vector to store all the legal moves
        append_legal_moves_in_selected_direction(legal_moves, direction, board);
    }
    return legal_moves;
}

// **********************
// Pawn
// Constructor for Rook
Pawn::Pawn(std::pair<int, int> start_position, color_t color) : Piece(start_position, color)
{
    if (color == color_t::black)
        symbol = "\u2659";
    else
        symbol = "\u265F";
}
Pawn::~Pawn() {}

// Move method for Rook. Simply move to the selected position
// The Rook is responsible for knowing if the move is a legal one.
void Pawn::move(std::pair<int, int> new_position)
{
    position = new_position;
}

// A method to obtain all legal moves given the current position
std::vector<std::pair<int, int>> Pawn::get_legal_moves(const std::array<Piece *, 64> &board)
{
    std::vector<std::pair<int, int>> legal_moves;
    int x = position.first;
    int y = position.second;
    Piece *piece_ptr;
    // dx is the direction the pawn goes. It is 1 for white and -1 for black
    int dy;
    if (color == color_t::white)
        dy = 1;
    else
        dy = -1;

    // One step ahead
    std::pair<int, int> one_step(x, y + dy);
    piece_ptr = board[ind(one_step)];

    if (y > 0 && y <= 7 && piece_ptr == nullptr)
    {
        legal_moves.push_back(one_step);
    }
    // If The pawn is in start position you can make two steps ahead
    std::pair<int, int> two_step(x, y + 2 * dy);
    piece_ptr = board[ind(two_step)];
    if ((y == 1 || y == 6) && piece_ptr == nullptr)
        legal_moves.push_back(two_step);

    // Check diagonals
    if (x < 7 && y < 7 && y > 0)
    {
        std::pair<int, int> right_step(x + 1, y + dy);
        piece_ptr = board[ind(right_step)];
        if (piece_ptr != nullptr && piece_ptr->get_color() != color)
            legal_moves.push_back(right_step);
    }
    if (x > 0 && y < 7 && y > 0)
    {
        std::pair<int, int> left_step(x - 1, y + dy);
        piece_ptr = board[ind(left_step)];
        if (piece_ptr != nullptr && piece_ptr->get_color() != color)
            legal_moves.push_back(left_step);
    }
    return legal_moves;
}

// **********************
// Queen
// Constructor for Rook
Queen::Queen(std::pair<int, int> start_position, color_t color) : Piece(start_position, color)
{
    if (color == color_t::black)
        symbol = "\u2655";
    else
        symbol = "\u265B";
}
Queen::~Queen() {}

// Move method for Rook. Simply move to the selected position
// The Rook is responsible for knowing if the move is a legal one.
void Queen::move(std::pair<int, int> new_position)
{
    position = new_position;
}

// A method to obtain all legal moves given the current position
std::vector<std::pair<int, int>> Queen::get_legal_moves(const std::array<Piece *, 64> &board)
{
    std::vector<std::pair<int, int>> legal_moves;
    // Run through all the directions since Queen
    for (std::pair<int, int> direction : cardinals)
    {
        // Vector to store all the legal moves
        append_legal_moves_in_selected_direction(legal_moves, direction, board);
    }
    for (std::pair<int, int> direction : diagonals)
    {
        // Vector to store all the legal moves
        append_legal_moves_in_selected_direction(legal_moves, direction, board);
    }
    return legal_moves;
}

// **********************
// King
// Constructor for Rook
King::King(std::pair<int, int> start_position, color_t color) : Piece(start_position, color)
{
    if (color == color_t::black)
        symbol = "\u2654";
    else
        symbol = "\u265A";
}
King::~King() {}

// Move method for Rook. Simply move to the selected position
// The Rook is responsible for knowing if the move is a legal one.
void King::move(std::pair<int, int> new_position)
{
    position = new_position;
}

// A method to obtain all legal moves given the current position
std::vector<std::pair<int, int>> King::get_legal_moves(const std::array<Piece *, 64> &board)
{
    std::vector<std::pair<int, int>> legal_moves;
    // Run through all the directions since King
    Piece *piece_ptr;
    for (std::pair<int, int> direction : cardinals)
    {
        int x = position.first + direction.first;
        int y = position.second + direction.second;
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            piece_ptr = board[ind({x, y})];
            int status = check_status_of_piece(piece_ptr);
            if (status != 1)
            {
                if (!check_if_square_threatened({x, y}, board))
                    legal_moves.push_back({x, y});
            }
        }
    }
    for (std::pair<int, int> direction : diagonals)
    {
        int x = position.first + direction.first;
        int y = position.second + direction.second;
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            piece_ptr = board[ind({x, y})];
            int status = check_status_of_piece(piece_ptr);
            if (status != 1)
            {
                if (!check_if_square_threatened({x, y}, board))
                    legal_moves.push_back({x, y});
            }
        }
    }
    return legal_moves;
}

// *****    *****************
// Knight
// Constructor for Rook
Knight::Knight(std::pair<int, int> start_position, color_t color) : Piece(start_position, color)
{
    if (color == color_t::black)
        symbol = "\u2658";
    else
        symbol = "\u265E";
}
Knight::~Knight() {}

// Move method for Rook. Simply move to the selected position
// The Rook is responsible for knowing if the move is a legal one.
void Knight::move(std::pair<int, int> new_position)
{
    position = new_position;
}

// A method to obtain all legal moves given the current position
std::vector<std::pair<int, int>> Knight::get_legal_moves(const std::array<Piece *, 64> &board)
{
    int x = position.first;
    int y = position.second;
    // These are the possible moves a knight can make
    std::array<std::pair<int, int>, 8> possible_moves = {std::make_pair(x + 2, y + 1),
                                                         std::make_pair(x + 1, y + 2),
                                                         std::make_pair(x - 1, y + 2),
                                                         std::make_pair(x - 2, y + 1),
                                                         std::make_pair(x - 2, y - 1),
                                                         std::make_pair(x - 1, y - 2),
                                                         std::make_pair(x + 1, y - 2),
                                                         std::make_pair(x + 2, y - 1)};

    // A vector for holding the legal moves available to this knight now
    std::vector<std::pair<int, int>> legal_moves;
    for (std::pair<int, int> move : possible_moves)
    {
        append_legal_move_in_selected_square(legal_moves, move, board);
    }
    return legal_moves;
}

// **********************
// Bishop
// Constructor for Rook
Bishop::Bishop(std::pair<int, int> start_position, color_t color) : Piece(start_position, color)
{
    if (color == color_t::black)
        symbol = "\u2657";
    else
        symbol = "\u265D";
}
Bishop::~Bishop() {}

// Move method for Rook. Simply move to the selected position
// The Rook is responsible for knowing if the move is a legal one.
void Bishop::move(std::pair<int, int> new_position)
{
    position = new_position;
}

// A method to obtain all legal moves given the current position
std::vector<std::pair<int, int>> Bishop::get_legal_moves(const std::array<Piece *, 64> &board)
{
    std::vector<std::pair<int, int>> legal_moves;
    // Run through all the cardinal directions since Rook
    for (std::pair<int, int> direction : diagonals)
    {
        // Vector to store all the legal moves
        append_legal_moves_in_selected_direction(legal_moves, direction, board);
    }
    return legal_moves;
}
