#include "../headerfiles/board_printer.h"

// Function to give a string background color
std::string give_bg(std::string in_string, int color)
{
    // Colors for plain black ckecker
    if (color == 0)
    {
        return "\033[01;43;31m" + in_string + "\033[0m";
    }
    // Colors for plain white checker
    else if (color == 1)
    {
        return "\033[01;44;31m" + in_string + "\033[0m";
    }
    // Colors for selected black checker
    else if (color == 2)
    {
        return "\033[02;43;31m" + in_string + "\033[0m";
    }
    // Colors for selected white checker
    else if (color == 3)
    {
        return "\033[02;44;31m" + in_string + "\033[0m";
    }
    // Colors for threatened black checker
    else if (color == 4)
    {
        return "\033[03;42;31m" + in_string + "\033[0m";
    }
    // Colors for threatened white checker
    else
    {
        return "\033[03;45;31m" + in_string + "\033[0m";
    }
}

// Method to print board
void BoardPrinter::print_board(Board &board)
{
    std::cout << "Player: " << color_s << "'s turn!" << std::endl;
    for (int y = 7; y >= 0; y--)
    {
        std::cout << " " << y + 1 << " ";
        for (int x = 0; x < 8; x++)
        {
            Piece *piece_ptr;
            piece_ptr = board.getPiecePtr(std::make_pair(x, y));
            if (piece_ptr == nullptr)
                std::cout << give_bg("  ", (x + y) % 2);
            else
                std::cout << give_bg(piece_ptr->get_symbol(), (x + y) % 2) << give_bg(" ", (x + y) % 2);
        }
        std::cout << std::endl;
    }
    std::cout << "  ";
    for (char c : "ABCDEFGH")
    {
        std::cout << " " << c;
    }
    std::cout << std::endl;
}

// Method to print chess board, marking the selected piece and allowed moves
void BoardPrinter::print_board_selected(Piece *selected_piece_ptr, Board &board)
{
    std::cout << "Player: " << color_s << "'s turn!" << std::endl;
    auto legal_moves = selected_piece_ptr->get_legal_moves(board.getBoard());
    for (int y = 7; y >= 0; y--)
    {
        std::cout << " " << y + 1 << " ";
        for (int x = 0; x < 8; x++)
        {
            int checker = (x + y) % 2;
            Piece *piece_ptr;
            piece_ptr = board.getPiecePtr(std::make_pair(x, y));
            if (piece_ptr == selected_piece_ptr)
                checker += 2;
            // else if ("x and y is in legal_moves")
            else if (std::find(legal_moves.begin(),
                               legal_moves.end(),
                               std::make_pair(x, y)) != legal_moves.end())
            {
                checker += 4;
            }
            if (piece_ptr == nullptr)
                std::cout << give_bg("  ", checker);
            else
            {
                std::cout << give_bg(piece_ptr->get_symbol(), checker) << give_bg(" ", checker);
            }
        }
        std::cout << std::endl;
    }
    std::cout << "  ";
    for (char c : "ABCDEFGH")
    {
        std::cout << " " << c;
    }
    std::cout << std::endl;
}

void BoardPrinter::set_player(color_t color)
{
    if (color == white)
    {
        color_s = "white";
    }
    else
        color_s = "black";
}