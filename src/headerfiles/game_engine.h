#ifndef GAME_ENGINE_H
#define GAME_ENGINE_H
#include "../headerfiles/piece.h"
#include "../headerfiles/inputhandler.h"
#include "../headerfiles/board.h"
#include "../headerfiles/board_printer.h"
#include "../headerfiles/save_handler.h"


class GameEngine
{
private:
    void clear_screen();
    Player *switch_turn();

    SaveHandler *save_handler = nullptr;
    InputHandler *input = nullptr;
    Board *board_object = nullptr;
    BoardPrinter *board_printer;
    Piece *piece_Ptr;
    Player *current_player_ptr;
    std::array<Player *, 2> players;
    bool check_win();
    int quit();
    bool b_turn_change;

public:
    GameEngine(){};

    void init();
    bool game_loop();
    bool replay();
};
#endif