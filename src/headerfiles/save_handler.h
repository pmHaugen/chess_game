#ifndef SAVER_LOADER_H
#define SAVER_LOADER_H
#include "board.h"
#include <array>
#include <fstream>

class SaveHandler
{
private:
    //std::array<std::vector<std::pair<std::string, std::pair<int, int>>>, 10> savegames;
    std::array<std::vector<std::vector<std::pair<std::string, std::pair<int, int>>>>, 10> savegames_w_history;
public:
    SaveHandler();
    ~SaveHandler();
    // Method for saving the current state into buffer
    void save_board_state(Board &, int);
    // Method for saving the saves on file
    void write_savegames_to_file(std::string);
    // Method for loading the board state from savegame
    Board *load_board_state(int, int frame = -1);
    // Method for loading the current board from file
    void load_savegames_from_file(std::string);
    // Deletes the selected save slot
    void delete_save_slot(int);
    // Prints all the saves as a list
    void print_saves();
    // Gets valid save slots
    std::vector<int> get_valid_save_slots();
};

#endif