#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H
#include "../headerfiles/piece.h"
#include "../headerfiles/player.h"
#include "../headerfiles/board.h"
#include "../headerfiles/save_handler.h"
#include <array>

class InputHandler
{
private:
    int save_slot_index;

public:
    InputHandler();
    ~InputHandler();
    std::pair<int, int> pick_piece(Board *, Player *);
    std::pair<int, int> pick_move_location();
    std::pair<int, int> convert_to_valid(char, char);
    bool input_letter_checker(char);
    bool input_number_checker(char);
    void pick_save_slot(SaveHandler *);
    int get_save_slot() { return save_slot_index; }
};

#endif