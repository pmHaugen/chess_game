# Chess Game
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This is a chess game made with C++ as a group project for education. This chess game includes the standard rules chess rules for moving
pieces, win condition and general gameplay. The game features a hot seat system so you play human player againt another human.
Extra features includes:
-Saving and loading multiple save slots
-Replay

![alt picture](chess.png)
## Requirments
This project depends on
* [CMake](https://cmake.org)
* [Google tests](gttps://github.com/google/googletests)


## Install

To install the game you need be in a linux based environment. First you need do is to create a bin folder in root directory and run the "cmake ../" command. This sets up the makefile. Then you can enter the
bin folder and run the "make" command

Required installs:
```
apt-get update --yes
apt install --yes cmake
apt-get install --yes libgtest-dev       (for google testing)
```
Install the chess game:
```
mkdir bin
cd bin
cmake ../
make
```

## Usage
From the bin folder you can write the command ./main to start the program in the terminal. After running the program you can simply follow the on
screen commands to play the game. Coordinates are represented on the board to the left and under the board and can be used by typing in the format of:
A1, a1, a 1 or a enter 1. And pressing enter after entering the coordinates. The game is structured so you first pick the coordinate of the piece
you want to move, then the coordinates of where you want to place it. If your move is invalid or you do something not allowed like moving into check mate
you will be asked to try again.

```
Linux:
./main
```



### Contributing
*Joel Fredin
*Thomas Haaland
*Paal Marius Haugen

### Joel Fredin
Joel contributed mainly to making the board class, and doing testing of the pieces in the piece_tests.cpp file. He also
helped with commenting and documenting the code

### Thomas Haaland
Thomas contributed to making the logic in the pieces, classes, the board visualizer and the save and load
function. Thomas aslo laid the groundwork for testing

### Paal Marius Bruun Haugen
Paal Marius contributed mainly to making the game engine, input handler and setting up CI/CD pipeline testing. He also helped out
with planing and setting up the code structure


## License

