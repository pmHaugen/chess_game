# set cmake minimum version
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(Chessgame_Project VERSION 1.0)

# enable testing framework
enable_testing()

# specify the tests to be performed (and output)
add_test(NAME PieceTest
	COMMAND pieceTest --gtest_output=xml:report.xml)

# locate GTest and include it
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

# Link tests with what we want to test and the Googletest and pthread library
add_executable(pieceTests ./src/cppfiles/piece_tests.cpp ./src/cppfiles/piece.cpp ./src/cppfiles/board.cpp)
target_link_libraries(pieceTests ${GTEST_LIBRARIES} pthread)

# include and link source folder
include_directories(./src/cppfiles)
link_directories(./src/headerfiles)

# add the executable
add_executable(main ./src/cppfiles/main.cpp)

# add add library
add_library(Chessgame_Lib src/cppfiles/board_printer.cpp src/cppfiles/board.cpp src/cppfiles/game_engine.cpp src/cppfiles/inputhandler.cpp src/cppfiles/piece.cpp src/cppfiles/player.cpp src/cppfiles/save_handler.cpp)

# link the library to the executable
target_link_libraries(main Chessgame_Lib)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# make release the default build
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
  message(STATUS "Build type not specified: Use Release by default")
endif()

set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O")
